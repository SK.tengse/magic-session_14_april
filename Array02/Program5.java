/*
Program 5
WAP to take size of array from user and also take integer elements from user
find the minimum element from the array
input : Enter size : 5
Enter array elements: 1 2 5 0 4
output: min element = 0

Program 6
WAP to take size of array from user and also take integer elements from user
find the maximum element from the array
input : Enter size : 5
Enter array elements: 1 2 5 0 4
output: max element = 5
*/

import java.io.*;

class MinMax{
	
	public static void main(String[] C2W)throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter size of an array:");
		int size=Integer.parseInt(br.readLine());

		int[] arr =  new int[size];
		
		System.out.println("Enter elements of an array::");
		int min=0,max=0;
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
			
			if(arr[i]>max)
				max=arr[i];
			if(arr[i]<min)
				min=arr[i];
		}

		System.out.println("Minimun Element ="+min);
		System.out.println("Maximun Element ="+max);
			
		
		
	}

}
