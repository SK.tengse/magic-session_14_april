/*
 Write a Java program to merge two given arrays.
Array1 = [10, 20, 30, 40, 50]
Array2 = [9, 18, 27, 36, 45]
Output :
Merged Array = [10, 20, 30, 40, 50, 9, 18, 27, 36, 45]
Hint: you can take 3rd array
*/
 
import java.io.*;

class ArrayMeger{
	
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));	
		System.out.println("Enter size of arr1:");
		int size1=Integer.parseInt(br.readLine());
		
		System.out.println("Enter size of arr2:");
		int size2=Integer.parseInt(br.readLine());

		int[] arr1=new int[size1];
		int[] arr2=new int[size2];
		System.out.println("Enter elements in arr1:");
		for(int i=0;i<arr1.length;i++){
			arr1[i]=Integer.parseInt(br.readLine());	
		}
		System.out.println("Enter elements in arr2:");
		for(int i=0;i<arr2.length;i++){
			arr2[i]=Integer.parseInt(br.readLine());	
		}
		int l3=arr1.length+arr2.length;
		int[] arr3=new int[l3];

		for(int i=0;i<arr1.length;i++){
			arr3[i]=arr1[i];
		}	
		for(int i=0;i<arr2.length;i++){
			arr3[arr1.length+i]=arr2[i];
		}
		System.out.println("Elements in arr3:");
	
		for(int i=0;i<arr3.length;i++){
			System.out.println(arr3[i]);
		}	
	}

}
