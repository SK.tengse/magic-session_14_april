/*
Program 4
WAP to search a specific element from an array and return its index.
Input: 1 2 4 5 6
Enter element to search: 4
Output: element found at index: 2
*/

import java.io.*;

class ElementSearch{
	
	public static void main(String[] C2W)throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter size of an array:");
		int size=Integer.parseInt(br.readLine());
		
		int store=-1;

		int[] arr =  new int[size];
		
		System.out.println("Enter elements of an array::");
		
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		
		}

		System.out.println("Enter an element to search::");
		int key=Integer.parseInt(br.readLine());
		
		for(int i=0;i<arr.length;i++){
			if(arr[i]==key){
				store=i;
				break;				
			}	
		}
		if(store>=0){
			System.out.println("Search element found at "+store+" index");
		}else{
			System.out.println("Not Present In Index");		
		}
		
	}

}
