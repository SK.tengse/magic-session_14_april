/*
 
Program 2
WAP to find the number of even and odd integers in a given array of integers
Input: 1 2 5 4 6 7 8
Output:
Number of Even Elements: 4
Number of Odd Elements : 3

Program 3
Write a Java program to find the sum of even and odd numbers in an array.
Display the sum value.
Input: 11 12 13 14 15
Output
Odd numbers sum = 39
*/

import java.io.*;

class ArrayEvenOddSum{
	
	public static void main(String[] C2W)throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter size of an array:");
		int size=Integer.parseInt(br.readLine());

		int[] arr =  new int[size];
		int sumEven=0,sumOdd=0,cnt=0;
		System.out.println("Enter elements of an array::");
		
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
			if(arr[i]%2==0){
				sumEven=sumEven+arr[i];
				cnt++;
			}else
				sumOdd=sumOdd+arr[i];
		}

		System.out.println("Count of even elements in array:"+cnt);
		System.out.println("Count of odd elements in array:"+(arr.length-cnt));
		System.out.println("Sum of even elements in array:"+sumEven);
		System.out.println("Sum of odd elements in array:"+sumOdd);
	}

}
