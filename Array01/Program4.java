/*
Program 4
Write a program, take 7 characters as an input , Print only vowels from the array
Input: a b c o d p e
Output : a o e
*/

import java.io.*;

class charVowels{
	
	public static void main(String[] C2W)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Enter size of an array:");
		int size=Integer.parseInt(br.readLine());
		
		char[] Carr = new char[size];
		
		System.out.println("Enter array element::");
		
		for(int i=0;i<Carr.length;i++){
			
			Carr[i]=(char)br.read();
			br.skip(1);
			
		}
		for(int i =0;i<Carr.length;i++){
			
			if((Carr[i]>='A'||Carr[i]<='Z')||(Carr[i]>='a'||Carr[i]<='z')){
				if(Carr[i]=='a'||Carr[i]=='A')
					System.out.print(Carr[i]+"\t");
				if(Carr[i]=='e'||Carr[i]=='E')
					System.out.print(Carr[i]+"\t");
				if(Carr[i]=='i'||Carr[i]=='I')
					System.out.print(Carr[i]+"\t");
				if(Carr[i]=='o'||Carr[i]=='O')
					System.out.print(Carr[i]+"\t");
				if(Carr[i]=='u'||Carr[i]=='U')
					System.out.print(Carr[i]+"\t");
			}
			
		}
		System.out.println();
		
	}

}
