/*
Program 1
WAP to take size of array from user and also take integer elements from user Print sum
of odd elements only
input : Enter size : 5
Enter array elements : 1 2 3 4 5
output : 9
//1 + 3 + 5
*/

import java.io.*;

class oddSum{
	
	public static void main(String[] C2W)throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Enter size of an array::");	
		int size = Integer.parseInt(br.readLine());
		//to take array size from user
		
		int[] arr = new int[size];
		//asking for memory with new operator
		
		int sum=0;
		//variable for the sum of odd numbers

		System.out.println("Enter array elements::");
		for(int i=0;i<arr.length;i++){
			//arr.length is a variable that provides array size or length	
			
			arr[i]=Integer.parseInt(br.readLine());
			//to take input from user
			
			if(arr[i]%2!=0)
				sum=sum+arr[i];
			
		}
		System.out.println("Sum of odd numbers in given array is :"+sum);
	}
}
