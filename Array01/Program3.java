/*
Program 3
WAP to take size of array from user and also take integer elements from user Print
product of odd index only
input : Enter size : 6
Enter array elements : 1 2 3 4 5 6
output : 48
//2 * 4 * 6
*/
import java.io.*;

class oddIndexProduct{
	
	public static void main(String[] C2W)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Enter size of an array:");
		int size=Integer.parseInt(br.readLine());
		
		int[] arr = new int[size];
		int mult=1;
		//variable to store multiplication of even number
		
		System.out.println("Enter array element::");
		
		for(int i=0;i<arr.length;i++){
			
			arr[i]=Integer.parseInt(br.readLine());
			
			if(i%2!=0)
				mult=mult*arr[i];
		}
		System.out.println("Multiplication of odd index elements in given array::"+mult);

	}

}
