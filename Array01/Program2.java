/*
Program 2
WAP to take size of array from user and also take integer elements from user Print
product of even elements only
input : Enter size : 9
Enter array elements : 1 2 3 2 5 10 55 77 99
output : 40
// 2 * 2 * 10
*/
import java.io.*;

class evenProduct{
	
	public static void main(String[] C2W)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Enter size of an array:");
		int size=Integer.parseInt(br.readLine());
		
		int[] arr = new int[size];
		int mult=1;
		//variable to store multiplication of even number
		
		System.out.println("Enter array element::");
		
		for(int i=0;i<arr.length;i++){
			
			arr[i]=Integer.parseInt(br.readLine());
			
			if(arr[i]%2==0)
				mult=mult*arr[i];
		}
		System.out.println("Multiplication of even numbers in given array::"+mult);

	}

}
