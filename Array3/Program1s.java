/*
Program 1
Write a program to print count of digits in elements of array.
Input: Enter array elements : 02 255 2 1554
Output: 2 3 1 4
*/

import java.io.*;

class countDigits{

	public static void main(String[] args)throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter size :");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter the elemets in array:");

		for(int i=0;i<arr.length;i++)
			arr[i]=Integer.parseInt(br.readLine());

		for(int i=0;i<arr.length;i++){
			int cnt=0,rev=0;
			int temp =arr[i];
			while(temp!=0){
				int rem=temp%10;
				rev=rev*10+rem;
				temp=temp/10;
			}
			System.out.println(rev);
			while(rev!=0){
				cnt++;
				rev=rev/10;
			}	
			
			System.out.println(cnt);
		}
	}
}
